<!DOCTYPE html>
<html lang="de">
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/head.php';
?>

<body>
<div id="wrapper">
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/navigation.php';
    ?>
</div>
<div>
		<header id="logo" class="container-fluid">
			<img src="../assets/img/logo-myTaxi.png">
			<!--  Template ergänzen um das Logo-->
		</header>
</div>


<section class="container">
<main>

<h1>Allgemeine Geschäftsbedingungen</h1>
 
AGB erstellt über den Generator der Deutschen Anwaltshotline AG

<h2>Vertragspartner</h2>

Auf Grundlage dieser Allgemeinen Geschäftsbedingungen (AGB) kommt zwischen dem Kunden und 
myTaxi
Vertreten durch Muster Muster
Adresse: Muster Muster Muster 00000 Muster

Handelsregister: Muster
Handelsregisternummer: Muster
Umsatzsteuer-Identifikationsnummer: Muster, nachfolgend Anbieter genannt, der Vertrag zustande.
<hr>
<h2>Vertragsgegenstand</h2>
Durch diesen Vertrag wird der Verkauf von Dienstleistungen über den Online-Shop des Anbieters geregelt. Wegen der Details des jeweiligen Angebotes wird auf die Produktbeschreibung der Angebotsseite verwiesen.
<hr>
<h2>Vertragsschluss</h2>

Der Vertrag kommt im elektronischen Geschäftsverkehr über das Shop-System oder über andere Fernkommunikationsmittel wie Telefon und E-Mail zustande. Dabei stellen die dargestellten Angebote eine unverbindliche Aufforderung zur Abgabe eines Anbgebots durch die Kundenbestellung dar, das der Anbieter dann annehmen kann. Der Bestellvorgang zum Vertragsschluss umfasst im Shop-System folgende Schritte:
    Auswahl des Angebots in der gewünschten Spezifikation (Größe, Farbe, Anzahl)
    Einlegen des Angebots in den Warenkorb
    Betätigen des Buttons 'bestellen'
    Eingabe der Rechnungs- und Lieferadresse
    Auswahl der Bezahlmethode
    Überprüfung und Bearbeitung der Bestellung und aller Eingaben
    Betätigen des Buttons 'kostenpflichtig bestellen'
    Bestätigungsmail, dass Bestellung eingegangen ist Bestellungen können neben dem Shop-System auch über Fernkommunikationsmittel (Telefon/E-Mail) aufgegeben werden, wodurch der Bestellvorgang zum Vertragsschluss folgende Schritte umfasst:
    Anruf bei der Bestellhotline / Übersendung der Bestellmail
    Bestätigungsmail, dass Bestellung eingegangen ist Die Bestellung der Leistung ist kostenpflichtig. Es entstehen Kosten in Höhe von: 12€/Goldbarren Mit der Zusendung der Bestellbestätigung kommt der Vertrag zustande.
<hr>
<h2>Vertragsdauer</h2>
Der Vertrag hat vorbehaltlich einer Kündigung eine Laufzeit von 1 Wochen. Der Gesamtpreis errechnet sich aus den folgenden Komponenten: multiplikation
<hr>
<h2>Preise, Versandkosten, Rücksendekosten</h2>
Alle Preise sind Endpreise und enthalten die gesetzliche Umsatzsteuer. Neben den Endpreisen fallen je nach Versandart weitere Kosten an, die vor Versendung der Bestellung angezeigt werden. Besteht ein Widerrufsrecht und wird von diesem Gebraucht gemacht, trägt der Kunde die Kosten der Rücksendung.
<hr>
<h2>Zahlungsbedingungen</h2>
Der Kunde hat ausschließlich folgende Möglichkeiten zur Zahlung: Vorabüberweisung, Rechnung bei Lieferung, Lastschrifteinzug, Nachnahme, Zahlungsdienstleister (PayPal), Barzahlung bei Abholung, Kreditkarte. Weitere Zahlungsarten werden nicht angeboten und werden zurückgewiesen.
Der Rechnungsbetrag ist nach Zugang der Rechnung, die alle Angaben für die Überweisung enthält und mit E-Mail verschickt wird, auf das dort angegebene Konto vorab zu überweisen. Der Rechnungsbetrag ist nach Zugang der Rechnung, die alle Angaben für die Überweisung enthält und mit der Lieferung verschickt wird, auf das dort angegebene Konto vorab zu überweisen. Der Rechnungsbetrag wird vom Anbieter mittels Lastschriftverfahren auf Grundlage der Einzugsermächtigung durch den Kunden von dessen angegebenem Konto eingezogen. Bei Lieferung gegen Nachnahme wird der Nachnahmebetrag bei Zustellung bar an den Zusteller gezahlt, wobei der Zusteller eine Nachnahmegebühr erhebt. Bei Verwendung eines Treuhandservice/ Zahlungsdienstleisters ermöglicht es dieser dem Anbieter und Kunden, die Zahlung untereinander abzuwickeln. Dabei leitet der Treuhandservice/ Zahlungsdienstleister die Zahlung des Kunden an den Anbieter weiter. Weitere Informationen erhalten Sie auf der Internetseite des jeweiligen Treuhandservices/ Zahlungsdienstleisters. Der Rechnungsbetrag kann auch unter Abzug der in Ansatz gebrachten Versandkosten in den Geschäftsräumen des Anbieters zu den üblichen Bürozeiten in bar gezahlt werden. Bei Zahlung über Kreditkarte muss der Kunde der Karteninhaber sein. Die Belastung der Kreditkarte erfolgt nach Versand der Ware. Der Kunde ist verpflichtet innerhalb von 14 Tagen nach Erhalt der Rechnung den ausgewiesenen Betrag auf das auf der Rechnung angegebene Konto einzuzahlen oder zu überweisen. Die Zahlung ist ab Rechnungsdatum ohne Abzug fällig. Der Kunde kommt erst nach Mahnung in Verzug.
<hr>
<h2>Lieferbedingungen</h2>
Die Ware wird umgehend nach Eingang der Bestellung versandt. Der Versand erfolgt durchschnittlich spätestens nach 14 Tagen. Der Unternehmer verpflichtet sich zur Lieferung am 14 Tag nach Bestelleingang. Die Regellieferzeit beträgt 14 Tage, wenn in der Artikelbeschreibung nichts anderes angegeben ist. Der Anbieter versendet die Bestellung aus eigenem Lager, sobald die gesamte Bestellung dort vorrätig ist.
<hr>
<h2>Vertragsgestaltung</h2>
Der Kunde hat keine Möglichkeit selbst direkt auf den gespeicherten Vertragstext zuzugreifen.
<hr>
<h2>Widerrufsrecht und Kundendienst</h2>
<h3>Widerrufsbelehrung</h3>
<h3>Widerrufsrecht</h3>

Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gründen diesen Vertrag zu widerrufen.

Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag, 
•	Im Falle eines Kaufvertrags: an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die letzte Ware in Besitz genommen haben bzw. hat.
•	Im Falle einer Vertrags über mehrere Waren, die der Verbraucher im Rahmen einer einheitlichen Bestellung bestellt hat und die getrennt geliefert werden: an dem Sie oder ein von Ihnen benannter Dritter, der nicht Beförderer ist, die letzte Ware in Besitz genommen haben bzw. hat.
•	Im Falle eines Vertrags über die Lieferung einer Ware in mehreren Teilsendungen oder Stücken: an dem Sie oder ein von Ihnen benannter Dritter, der nicht Beförderer ist, die letzte Teilsendung oder das letzte Stück in Besitz genommen haben bzw. hat.
•	Im Falle eines Vertrages zur regelmäßigen Lieferung von Waren über einen festgelegten Zeitraum hinweg: an dem Sie oder ein von Ihnen benannter Dritter, der nicht Beförderer ist, die erste Ware in Besitz genommen haben bzw. hat.
Beim Zusammentreffen mehrerer Alternativen ist der jeweils letzte Zeitpunkt maßgeblich.

Um Ihr Widerrufsrecht auszuüben, müssen Sie uns (myTaxi, Muster Muster, Muster Muster 00000 Muster) mittels einer eindeutigen Erklärung (z.B. ein mit der Post versandter Brief, Telefax, oder E-Mail) über Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. Sie können dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist.

Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.

Folgen des Widerrufs

Wenn Sie diesen Vertag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahmen der zusätzlichen Kosten, die sich daraus ergeben, dass Sie einer andere Art der Lieferung als die von uns angebotene, günstige Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. Wir können die Rückzahlung verweigern, bis wir die Waren wieder zurückerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zurückgesandt haben, je nachdem, welches der frühere Zeitpunkt ist.

Sie haben die Waren unverzüglich und in jedem Fall spätestens binnen vierzehn Tagen ab dem Tag, an dem Sie uns über den Widerruf dieses Vertrags unterrichten, an myTaxi, Muster Muster, Muster Muster 00000 Muster uns zurückzusenden oder zu übergeben. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden.

Sie tragen die unmittelbaren Kosten der Rücksendung der Waren.

Sie müssen für einen etwaigen Wertverlust der Waren nur aufkommen, wenn dieser Wertverlust auf einen zur Prüfung der Beschaffenheit, Eigenschaften und Funktionsweise der Waren nicht notwendigen Umgang mit ihnen zurückzuführen ist.

Ende der Widerrufsbelehrung
<hr>
<h2>Sprache, Gerichtsstand und anzuwendendes Recht</h2>
Der Vertrag wird in Deutsch abgefasst. Die weitere Durchführung der Vertragsbeziehung erfolgt in Deutsch. Es findet ausschließlich das Recht der Bundesrepublik Deutschland Anwendung. Für Verbraucher gilt dies nur insoweit, als dadurch keine gesetzlichen Bestimmungen des Staates eingeschränkt werden, in dem der Kunde seinen Wohnsitz oder gewöhnlichen Aufenthalt hat. Gerichtsstand ist bei Streitigkeiten mit Kunden, die kein Verbraucher, juristische Person des öffentlichen Rechts oder öffentlich-rechtliches Sondervermögen sind, Sitz des Anbieters. 

<hr>
</main>
</section>

<section>
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
    ?>
</section>

	<!-- jQuery library -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<!-- Latest compiled Bootstrap JavaScript -->
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>