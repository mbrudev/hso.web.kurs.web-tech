<!DOCTYPE html>
<html>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/head.php';
?>

<body>
<section>
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/navigation.php';
    ?>
</section>

<section>
    <div class="container-fluid">
            <img class="img-responsive" src="../assets/img/logoTaxi.jpg">
    </div>
</section>

<section>
    <main>
        <div class="container text-center">
            <div>
                <h1>Fakten über Uns</h1>
                <p>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
                    labore et
                    dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
                    rebum.
                    Stet
                    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit
                    amet,
                    consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam
                    erat, sed
                    diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,
                    no
                    sea
                    takimata sanctus est Lorem ipsum dolor sit amet.
                </p>
            </div>
            <div id="tabFolder_fakten" data-section="counters">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#1" data-toggle="tab">Kunden</a></li>
                    <li><a href="#2" data-toggle="tab">Reservierungsentwicklung</a></li>
                    <li><a href="#3" data-toggle="tab">Flotte</a></li>
                    <li><a href="#4" data-toggle="tab">Umsatz</a></li>
                </ul>

                <div class="tab-content">
                    <!-- http://www.blindtextgenerator.de/ -->
                    <div class="tab-pane active" id="1">
                        <h3>Kundenentwicklung</h3>
                        <img class="img-responsive" src="../assets/facts/2017Kundenentwicklung.jpg">

                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                            <tr>
                                <th></th>
                                <th>2013</th>
                                <th>2014</th>
                                <th>2015</th>
                                <th>2016</th>
                                <th>2017</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>privat</td>
                                <td>2</td>
                                <td>10</td>
                                <td>30</td>
                                <td>100</td>
                                <td>145</td>
                            </tr>
                            <tr>
                                <td>gwerblich</td>
                                <td>22</td>
                                <td>45</td>
                                <td>67</td>
                                <td>101</td>
                                <td>176</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="tab-pane" id="2">
                        <h3>Reservierungsentwicklung</h3>
                        <img class="img-responsive" src="../assets/facts/2017Reservierungsentwicklung.jpg">

                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                            <tr>
                                <th></th>
                                <th>2013</th>
                                <th>2014</th>
                                <th>2015</th>
                                <th>2016</th>
                                <th>2017</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>privat</td>
                                <td>1200</td>
                                <td>2000</td>
                                <td>2900</td>
                                <td>4000</td>
                                <td>6000</td>
                            </tr>
                            <tr>
                                <td>gwerblich</td>
                                <td>3000</td>
                                <td>4800</td>
                                <td>6000</td>
                                <td>7300</td>
                                <td>9000</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="tab-pane" id="3">
                        <h3>Flotte</h3>
                        <img class="img-responsive" src="../assets/facts/2017Verteilung_Flotte.jpg">

                        <table class="table table-bordered table-striped table-responsive">
                            <tbody>
                            <tr>
                                <td>Zweisitzer</td>
                                <td>7%</td>
                            </tr>
                            <tr>
                                <td>Viersitzer</td>
                                <td>43%</td>
                            </tr>
                            <tr>
                                <td>Sechssitzer</td>
                                <td>15%</td>

                            </tr>
                            <tr>
                                <td>Neunsitzer</td>
                                <td>15%</td>

                            </tr>
                            <tr>
                                <td>Minibus</td>
                                <td>20%</td>

                            </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class="tab-pane" id="4">
                        <h3>Umsatz</h3>
                        <img class="img-responsive" src="../assets/facts/2017Umsatzentwicklung.jpg">

                        <table class="table table-bordered table-striped table-responsive">
                            <thead>
                            <tr>
                                <th></th>
                                <th>2013</th>
                                <th>2014</th>
                                <th>2015</th>
                                <th>2016</th>
                                <th>2017</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>privat</td>
                                <td>60000€</td>
                                <td>100000€</td>
                                <td>145000€</td>
                                <td>200000€</td>
                                <td>300000€</td>
                            </tr>
                            <tr>
                                <td>gwerblich</td>
                                <td>150000€</td>
                                <td>240000€</td>
                                <td>300000€</td>
                                <td>365000€</td>
                                <td>450000€</td>
                            </tr>
                            <tr>
                                <td>Summe</td>
                                <td>210000€</td>
                                <td>340000€</td>
                                <td>445000€</td>
                                <td>565000€</td>
                                <td>750000€</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </main>
</section>

<section>
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
    ?>
</section>
</body>
</html>