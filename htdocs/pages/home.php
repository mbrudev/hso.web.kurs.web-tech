<!DOCTYPE html>
<html>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/head.php';
?>

<body>
<section>

    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/navigation.php';
    ?>
</section>
<div id="logoSlider" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#logoSlider" data-slide-to="0" class="active"></li>
        <li data-target="#logoSlider" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active">
            <img class="img-responsive" src="../assets/img/pink.jpg" alt="pink" style="width: 100%; height: 400px">
        </div>
        <div class="item">
            <img class="img-responsive" src="../assets/img/yellow.jpg" alt="yellow"
                 style="width: 100%; height: 400px">
        </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#logoSlider" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>

    </a>
    <a class="right carousel-control" href="#logoSlider" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>

    </a>
</div>


<section>
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
    ?>
</section>

</body>
</html>