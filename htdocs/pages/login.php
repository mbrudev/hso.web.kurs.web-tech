<!DOCTYPE html>
<html lang="de">
<?php include $_SERVER['DOCUMENT_ROOT'].'/templates/head.php';?>
<body>
<div id="wrapper">
    <?php include $_SERVER['DOCUMENT_ROOT'].'/templates/navigation.php';?>
</div>


<section class="container">
    <h1>Hello World</h1>
</section>

<section>
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
    ?>
</section>

<!-- jQuery library -->
<script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<!-- Latest compiled Bootstrap JavaScript -->
<script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>