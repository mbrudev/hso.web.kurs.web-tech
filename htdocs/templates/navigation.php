<nav class="navbar navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#navbar">
                <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo '/pages/home.php'; ?>">Taxi Taxi
            </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar">

            <ul id="nav1" class="nav navbar-nav">
                <li><a href="<?php echo '/pages/home.php'; ?>">Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo '/pages/about.php'; ?>">Über
                        Uns<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo '/pages/about.php#default'; ?>">Allgemeines</a></li>
                        <li><a href="<?php echo '/pages/about.php#idea'; ?>">Geschäftsidee</a></li>
                        <li><a href="<?php echo '/pages/about.php#location'; ?>">Standorte</a></li>
                        <li><a href="<?php echo '/pages/about.php#use'; ?>">Mehrwert</a></li>
                        <li><a href="<?php echo '/pages/about.php#partner'; ?>">Unsere Partner</a></li>
                        <li><a href="<?php echo '/pages/about.php#feedback'; ?>">Rezessionen</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo '/pages/facts.php'; ?>">Fakten</a></li>
                    </ul>
                </li>

                <li><a href="<?php echo '/pages/price.php'; ?>">Preise</a></li>
                <li><a href="<?php echo '/pages/reservation.php'; ?>">Reservierung</a></li>
                <li><a href="<?php echo '/pages/pitches.php'; ?>">Stellplätze</a></li>
                <li><a href="<?php echo '/pages/sorts-of.php'; ?>">Allerlei</a></li>


            </ul>
            <div>
                <form class="navbar-form navbar-right">
                    <div class="form-group">
                        <input type="search" class="form-control" placeholder="Suchen">
                    </div>
                    <button type="submit" class="btn btn-default">
                        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                    </button>
                </form>
            </div>

            <ul id="nav2" class="nav navbar-nav navbar-right">
                <li><a href="<?php echo '/pages/login.php'; ?>"><span
                                class="glyphicon glyphicon-user"></span> mein Konto</a></li>
                <li><a href="<?php echo '/pages/contact.php'; ?>"><span
                                class="glyphicon glyphicon-envelope"></span> Kontakt</a></li>
            </ul>
        </div>
    </div>
</nav>