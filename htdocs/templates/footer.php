 <div id ="mb-footer" class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p><a href=""></a>Follow us on:</p>
                </div>
                <div class="col-md-12">
                    <ul class="list-inline">
                        <li><a class="fa fa-facebook-official" href="#"><span class=""></span></a></li>
                        <li><a class="fa fa-twitter" href="#"><span class=""></span></a></li>
                        <li><a class="fa fa-instagram" href="#"><span class=""></span></a></li>
                        <li><a class="fa fa-youtube-play" href="#"><span class=""></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h4>header4</h4>
                    <ul>
                        <li><a class="" href="#" title="">Link1</a></li>
                        <li><a class="" href="#" title="">Link2</a></li>
                        <li><a class="" href="#" title="">Link3</a></li>
                    </ul>
                </div>

                <div class="col-md-4">
                    <h4>header4</h4>
                    <ul>
                        <li><a class="" href="#" title="">Link1</a></li>
                        <li><a class="" href="#" title="">Link2</a></li>
                        <li><a class="" href="#" title="">Link3</a></li>
                    </ul>
                </div>


                <div class="col-md-4">
                    <h4>header4</h4>
                    <ul class="">
                        <li><a class="" href="#" title="">About us</a></li>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <a class="" href="mailto:info@email.com" title="Email: Infinitythinks">info@email.com</a>
                        </li>
                        <li>
                            <i class="fa fa-file-text-o"></i>
                            <a class="" href="#" title="">Contact Form</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <ul class="list-inline">
                        <li><a href="<?php echo '/pages/imprint.php'; ?>"><span class="">Imprint</span></a></li>
                        <li><a href="<?php echo '/pages/privacy-policy.php'; ?>"><span class="">Privacy & Policy</span></a></li>
                        <li><a href="<?php echo '/pages/terms-of-use.php'; ?>"><span class="">Terms of Use</span></a></li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <span>Copyright &copy; 2017 TaxiTaxi All Rights Reserved</span>
                </div>
            </div>
        </div>
    </div>

<!-- <!-- jQuery library -->-->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
 <!-- Latest compiled Bootstrap JavaScript -->
 <script type="text/javascript" src="<?php echo '/js/jquery-3.2.1.min.js'; ?>"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

