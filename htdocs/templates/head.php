<head>
    <title>My Taxi</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <meta name="language" content="de">
    <meta name="keywords" content="Taxi, Fahrtenservice">
    <meta name="description" content="Taxi">
    <meta name="robots" content="index,follow">
    <meta name="audience" content="alle">
    <meta name="page-topic" content="Dienstleistungen">
    <meta name="revisit-after" CONTENT="7 days">

    <!-- Latest compiled and minified Bootstrap-CSS -->
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="<?php echo '/css/default.css'; ?>">
</head>